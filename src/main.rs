// Copyright 2023, Nicholas Lotz
/*
This file is part of netlef.
netlef is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
netlef is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License along with netlef. If not, see <https://www.gnu.org/licenses/>.
*/

mod block;
mod blockchain;

use blockchain::Blockchain;

fn main() {
    let mut blockchain = Blockchain::new();

    // Add blocks to the blockchain and print messages
    blockchain.add_block("This is the first block after genesis.");
    blockchain.add_block("This is the second block after genesis.");

    // Print the blocks 
    for block in &blockchain.blocks {
        println!("{:#?}", block);
    }
}