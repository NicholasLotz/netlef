// Copyright 2023, Nicholas Lotz
/*
This file is part of netlef.
netlef is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
netlef is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License along with netlef. If not, see <https://www.gnu.org/licenses/>.
*/

use crate::block::{Block, calculate_hash};


// The Blockchain struct represents a simple blockchain containing a list of blocks
pub struct Blockchain {
    pub blocks: Vec<Block>,
}

impl Blockchain {
    // Creates a new blockchain with a genesis block
    pub fn new() -> Blockchain {
        let genesis_block = create_genesis_block();
        Blockchain {
            blocks: vec![genesis_block],
        }
    }

    // Adds a new block to the blockchain with the given message, if the block is valid
    pub fn add_block(&mut self, message: &str) {
        let previous_block = self.blocks.last().unwrap();
        let new_block = Block::new(previous_block.index + 1, message, &previous_block.hash);
        if is_valid_block(&new_block, previous_block) {
            self.blocks.push(new_block);
        }
    }
}

// Creates the genesis block, which is the first block in the blockchain
fn create_genesis_block() -> Block {
    Block::new(0, "Hello, World!", "0")
}

// Verifies if a new block is valid based on its hash and its connection to the previous block
fn is_valid_block(new_block: &Block, previous_block: &Block) -> bool {
    // Ensure the new block's previous hash matches the hash of the previous block
    if new_block.previous_hash != previous_block.hash {
        return false;
    }

    // Ensure the new block's hash is valid by recalculating it using the block's data and nonce
    let hash = calculate_hash(new_block.index, new_block.timestamp, &new_block.message, &new_block.previous_hash, new_block.nonce);
    if hash != new_block.hash {
        return false;
    }

    true
}

