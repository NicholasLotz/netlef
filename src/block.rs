// Copyright 2023, Nicholas Lotz
/*
This file is part of netlef.
netlef is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
netlef is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License along with netlef. If not, see <https://www.gnu.org/licenses/>.
*/

use std::time::{SystemTime, UNIX_EPOCH};
use sha2::{Sha256, Digest};

// The Block struct represents a block in the blockchain
#[derive(Debug)]
pub struct Block {
    pub index: u32, // The block's index in the blockchain
    pub timestamp: u128, // The time at which the block was created
    pub message: String, // The block's message (replace this with actual transactions in a real-world application)
    pub previous_hash: String, // The hash of the previous block in the blockchain
    pub hash: String, // The hash of the current block
    pub nonce: u64, // The nonce used in the Proof-of-Work mining process
}

impl Block {
    // Creates a new block with the given index, message, and previous block hash
    pub fn new(index: u32, message: &str, previous_hash: &str) -> Block {
        let timestamp = current_time_millis();
        // Mine the block using Proof-of-Work and obtain the resulting hash and nonce
        let (hash, nonce) = mine_block(index, timestamp, message, previous_hash);
        
        Block {
            index,
            timestamp,
            message: message.to_string(),
            previous_hash: previous_hash.to_string(),
            hash,
            nonce,
        }
    }
}

// Returns the current time in milliseconds since the UNIX epoch
fn current_time_millis() -> u128 {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards")
        .as_millis()
}

// Calculates the hash of a block using its index, timestamp, message, previous block hash, and nonce
pub fn calculate_hash(index: u32, timestamp: u128, message: &str, previous_hash: &str, nonce: u64) -> String {
    let input = format!("{}{}{}{}{}", index, timestamp, message, previous_hash, nonce);
    let mut hasher = Sha256::new();
    hasher.update(input);
    format!("{:x}", hasher.finalize())
}

// Mines a block using Proof-of-Work by finding a nonce that results in a hash with a specific number of leading zeros
// The difficulty is fixed at 4 leading zeros in this example, but it could be made adjustable in a real-world application
fn mine_block(index: u32, timestamp: u128, message: &str, previous_hash: &str) -> (String, u64) {
    let mut nonce = 0;
    let mut hash;

    loop {
        hash = calculate_hash(index, timestamp, message, previous_hash, nonce);
        if hash.starts_with("0000") { // Change the number of zeros to adjust the difficulty
            break;
        }
        nonce += 1;
    }

    (hash, nonce)
}
