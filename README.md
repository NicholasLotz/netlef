# netlef

This project seeks to develop a protocol that balances security, decentralization, and efficient long-distance communication. The design decision as of 2023 is to use a proof-of-work (POW) consensus algorithm.

The project is licensed under AGPLv3. See the [license](https://gitlab.com/NicholasLotz/netlef/-/blob/main/README.md) for more details.

**Note:** SHA-256 replaced MD5 hashing on 03 April 2023

![netlef_logo](images/netlef_space.png){width=40% height=40%}

Image created with the assistance of AI.

Copyright 2023, Nicholas Lotz


